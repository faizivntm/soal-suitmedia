class Blog {
  final String id;
  final String name;
  final String email;
  final String avatar;

  const Blog({
    required this.id,
    required this.name,
    required this.email,
    required this.avatar,
  });

  factory Blog.fromJson(Map<String, dynamic> json) {
    return Blog(
      id: json['id'],
      name: json['name'],
      email: json['email'],
      avatar: json['avatar'],
    );
  }
}
