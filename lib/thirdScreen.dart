import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:soal1/model.dart';
import 'package:soal1/repository.dart';
import 'package:soal1/secondScreen.dart';

class ThridScreen extends StatefulWidget {
  ThridScreen({Key? key, required this.name, required this.callback})
      : super(key: key);
  final String name;
  final callback;
  @override
  State<ThridScreen> createState() => _ThridScreenState();
}

class _ThridScreenState extends State<ThridScreen> {
  List<Blog> blog = [];
  Repository repository = Repository();

  getData() async {
    List<Blog> anu = await repository.getData();
    setState(() {
      blog = anu;
    });
  }

  @override
  void initState() {
    getData();

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String nm = '';
    return Scaffold(
      appBar: new AppBar(
          toolbarHeight: 60,
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios,
                color: Color.fromRGBO(85, 74, 240, 1)),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text(
            "Thrid Screen",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.black,
              fontSize: 18,
            ),
            textAlign: TextAlign.center,
          )),
      body: ListView.separated(
        itemBuilder: (context, index) {
          return Container(
              child: Column(children: [
            Container(
              child: Column(
                children: [
                  Row(children: [
                    Image.network(
                      blog[index].avatar,
                      height: 49,
                      width: 49,
                    ),
                    Column(children: <Widget>[
                      TextButton(
                          onPressed: (() {
                            Navigator.of(context).pop();
                            widget.callback(blog[index].name);
                          }),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                nm = blog[index].name,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                                textAlign: TextAlign.left,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                blog[index].email,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 10),
                                textAlign: TextAlign.left,
                              )
                            ],
                          )),
                    ]),
                  ]),
                ],
              ),
            ),
          ]));
        },
        separatorBuilder: (context, index) {
          return Divider();
        },
        itemCount: blog.length,
      ),
    );
  }
}
