import 'package:flutter/material.dart';
import './secondScreen.dart';
import 'dart:io';

class FirstScreen extends StatefulWidget {
  const FirstScreen({super.key});

  @override
  State<FirstScreen> createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  TextEditingController namaInput = TextEditingController();
  String a = '';
  String b = '';
  String c = 'Palindrome';

  void C() {
    setState(() {
      String stringToReverse = '${namaInput.text}';
      String reversedString = "";
      String beforeReverse = stringToReverse;
      c = a = b;
      for (int j = stringToReverse.length - 1; j >= 0; j--) {
        reversedString += stringToReverse[j];
      }
      print("Reversed String is");
      print(reversedString);
      if (beforeReverse == reversedString) {
        Text(a = 'isPalindrome');
      } else {
        Text(b = 'notPalindrome');
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void palindrome() {
    String stringToReverse = '${namaInput.text}';
    String reversedString = "";
    String beforeReverse = stringToReverse;
    for (int j = stringToReverse.length - 1; j >= 0; j--) {
      reversedString += stringToReverse[j];
    }
    print("Reversed String is");
    print(reversedString);
    if (beforeReverse == reversedString) {
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                content: SizedBox(child: Text(c = a = 'isPalindrome')),
              ));
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                content: SizedBox(child: Text(c = b = 'not palindrome')),
              ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/background.png'),
                  fit: BoxFit.cover)),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 186,
                ),
                Image(
                  image: AssetImage('assets/loginFoto.png'),
                  width: 116,
                  height: 116,
                ),
                SizedBox(height: 50),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 32, right: 33),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15)),
                  child: TextField(
                    controller: namaInput,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 32, right: 33),
                        counterText: '',
                        border: InputBorder.none,
                        hintText: 'nama',
                        hintStyle: TextStyle(fontSize: 16)),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                    height: 45,
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(left: 33, right: 33),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      margin: EdgeInsets.only(left: 33, right: 25, top: 13),
                      child: Column(
                        children: [
                          Text(
                            '$c',
                            style: TextStyle(fontSize: 16),
                          ),
                        ],
                      ),
                    )

                    // child: TextField(
                    //   decoration: InputDecoration(
                    //       contentPadding: EdgeInsets.only(left: 32, right: 33),
                    //       counterText: '',
                    //       border: InputBorder.none,
                    //       hintText: 'Palindrome',
                    //       hintStyle: TextStyle(fontSize: 16)),
                    // ),
                    ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 32, right: 33),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            palindrome();
                            C();
                          },
                          child: Text('CHECK'),
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Color.fromRGBO(3, 99, 123, 1),
                              minimumSize: Size(310, 41),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              )))
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 32, right: 33),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(100)),
                  child: Column(
                    children: [
                      ElevatedButton(
                          onPressed: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => SecondScreen(
                                          nama: namaInput.text,
                                        )));
                          },
                          child: Text('NEXT'),
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Color.fromRGBO(3, 99, 123, 1),
                            minimumSize: Size(310, 41),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                          ))
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
