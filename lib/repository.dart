import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:soal1/model.dart';

class Repository {
  final _baseUrl = "https://63c9fe89c3e2021b2d60fe5e.mockapi.io/Blog";

  Future getData() async {
    try {
      final response = await http.get(Uri.parse(_baseUrl));

      if (response.statusCode == 200) {
        print(response.body);
        Iterable it = jsonDecode(response.body);
        List<Blog> blog = it.map((e) => Blog.fromJson(e)).toList();
        return blog;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
