import 'package:flutter/material.dart';
import 'package:soal1/model.dart';
import './thirdScreen.dart';

class SecondScreen extends StatefulWidget {
  const SecondScreen({Key? key, required this.nama}) : super(key: key);
  final String nama;
  @override
  State<SecondScreen> createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  String nama = '';
  String acountName = 'Selected User Name';

  void setAccountName(nm) {
    setState(() {
      acountName = nm;
    });
  }

  @override
  void initState() {
    nama = widget.nama;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
            toolbarHeight: 60,
            centerTitle: true,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios,
                  color: Color.fromRGBO(85, 74, 240, 1)),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text(
              "Second Screen",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 18,
              ),
              textAlign: TextAlign.center,
            )),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                SizedBox(
                  height: 35,
                ),
                Container(
                  padding: EdgeInsets.only(left: 30, right: 30),
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Welcome',
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.only(left: 30, right: 30),
                  alignment: Alignment.topLeft,
                  child: Text(
                    '${nama}',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 250,
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    '${acountName}',
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 250,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 32, right: 33),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(100)),
                  child: Column(
                    children: [
                      ElevatedButton(
                          onPressed: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => ThridScreen(
                                        name: '', callback: setAccountName)));
                          },
                          child: Text(
                            'Choose a User',
                            style: TextStyle(fontSize: 14),
                          ),
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Color.fromRGBO(3, 99, 123, 1),
                            minimumSize: Size(310, 41),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                          ))
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
